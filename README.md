## Pasos para configurar la app:

```
git clone https://gitlab.com/lalibre/lalibre-ionic-radio-player.git

cd radio-player/

npm install howler

npm i @types/howler

npm install -g ionic/cli

npm install -g capacitor

ionic capacitor sync
```

for android

```
ionic capacitor build android 
```

or for iOS

```
ionic capacitor build ios
```

## Actualizción de Version strings

Edit: 
- VersionCode y versionName en package.json y android/app/gradle.build
- version en package.json
