### 0.1.3 (2022/May/12):
- Se incluye Kipa Radio (Pueblo Saraguro) 
- README.md en español

### 0.1.2 (2022/May/10):
- Pequeños ajustes en interfaz

### 0.1.1  (2022/May/09):

- Primera versión
- Radios la Voz de la CONFENIAE, Radio ñikanchi Mushkuy (OCKIL-Kichwa)



