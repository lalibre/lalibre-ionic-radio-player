import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'net.lalibre.radio',
  appName: 'LaLibre Radio',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
