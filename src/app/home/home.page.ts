import { Howl } from 'howler';
import { Component, ViewChild } from '@angular/core';
import { IonRange } from '@ionic/angular';

export interface Track {
  name: string,
  img: string,
  path: string
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {

  playlist: Track[] = [
    {
      name: 'La Voz de la CONFENIAE',
      img: './assets/img/lavozdelaconfeniae.jpg',
      path: 'https://radio.lalibre.net/confenaie'
    },
    {
      name: 'Ñukanchik Mushkuy',
      img: './assets/img/nukanchimushkuy.jpeg',
      path: 'https://radio.lalibre.net/nukanchimushkuy'
    },
    {
      name: 'Kipa Radio',
      img: './assets/img/kiparadio.jpg',
      path: 'https://radio.lalibre.net/kiparadio'
    }
  ];

  activeTrack: Track = null;
  player: Howl = null;
  isPlaying = false;
  progress =  0;
  @ViewChild('range', { static: false }) range: IonRange;

  constructor() {}

  start(track: Track) {

    if (this.player){
      this.player.stop();
    };

    this.player = new Howl({
      src: [track.path],
      html5: true,
      format: ['mp3', 'aac'],
      onplay: () => {
        this.isPlaying = true;
        this.activeTrack = track;
        this.updateProgress();
      },
      onend: () => {
        console.log('onend');
      },
      onload: () => {
        console.log("onload");
      },
      onloaderror: () => {
        console.log("onload error");
      },
      onplayerror: () => {
        console.log("onload error");
      }
    });
    this.player.play();
  }

  tooglePlayer(pause) {
    this.isPlaying = !pause;
    if (pause) {
      this.player.stop();
    } else{
      this.player.play();
    }
  }

  // prev() {
  //   let index = this.playlist.indexOf(this.activeTrack);
  //   if (index > 0) {
  //     this.start(this.playlist[index - 1]);
  //   } else {
  //     this.start(this.playlist[this.playlist.length - 1]);  
  //   }
  // }

  // next() {
  //   let index = this.playlist.indexOf(this.activeTrack);
  //   if (index != this.playlist.length - 1) {
  //     this.start(this.playlist[index + 1]);
  //   } else {
  //     this.start(this.playlist[0]);  
  //   } 
  // }

  seek() {
    let newValue = +this.range.value;
    let duration = this.player.duration();
    this.player.seek(duration * (newValue / 100));
  }

  updateProgress() {
     let seek = this.player.seek();
     let progress = (seek / this.player.duration()) * 100 || 0;
     this.progress = progress;
     setTimeout(() => {
       this.updateProgress();
     }, 1000);
  }

}
